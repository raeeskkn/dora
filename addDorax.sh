#!/bin/bash
basedir="`pwd`"

version=`basename $basedir`

if [ ! -z "$5" ]; then
	mkdir tmp5
	cd tmp5
	unzip $basedir/$5
	if [ ! -f DORAX_ENVDATA_DESC_TRANSLATION_hr.csv -a -f DORAX_ENVDATA_DESC_TRANSLATION_sh.csv ]; then
	   mv DORAX_ENVDATA_DESC_TRANSLATION_sh.csv DORAX_ENVDATA_DESC_TRANSLATION_hr.csv
	fi
	cd $basedir
fi

if [ ! -z "$4" ]; then
	mkdir tmp4
	cd tmp4
	unzip $basedir/$4
	if [ ! -f DORAX_TRANSLATION_hr.csv -a -f DORAX_TRANSLATION_sh.csv ]; then
	   mv DORAX_TRANSLATION_sh.csv DORAX_TRANSLATION_hr.csv
	fi
	cd $basedir
fi
if [ ! -z "$3" ]; then
	mkdir tmp3
	cd tmp3
	unzip $basedir/$3
	if [ ! -f DORAX_TRANSLATION_hr.csv -a -f DORAX_TRANSLATION_sh.csv ]; then
	   mv DORAX_TRANSLATION_sh.csv DORAX_TRANSLATION_hr.csv
	fi
	cd $basedir
fi
mkdir tmp2
cd tmp2
unzip $basedir/$2
if [ ! -f DORAX_TRANSLATION_hr.csv -a -f DORAX_TRANSLATION_sh.csv ]; then
   mv DORAX_TRANSLATION_sh.csv DORAX_TRANSLATION_hr.csv
fi
xdversion=$1
if [ -z "$xdversion" ]; then
   xdversion=$version
fi
for lang in bg cs da de el en es fi fr hr hu it ja ko nl pl pt ro ru sl sv tr zh; do
    echo "Processing $lang ..."
    if [ ! -f ../LOC-TXT-SD-$xdversion-$lang-orig.zip ]; then
		mv ../LOC-TXT-SD-$xdversion-$lang.zip ../LOC-TXT-SD-$xdversion-$lang-orig.zip
		unzip ../LOC-TXT-SD-$xdversion-$lang-orig.zip
		sed 1d DORAX_TRANSLATION_${lang}.csv >> masterdata/loc/XD_TRANSLATION_${lang}.csv
        if [ -f "../tmp3/DORAX_TRANSLATION_${lang}.csv" ]; then
			sed 1d ../tmp3/DORAX_TRANSLATION_${lang}.csv >> masterdata/loc/XD_TRANSLATION_${lang}.csv
		fi
        if [ -f "../tmp4/DORAX_TRANSLATION_${lang}.csv" ]; then
			sed 1d ../tmp4/DORAX_TRANSLATION_${lang}.csv >> masterdata/loc/XD_TRANSLATION_${lang}.csv
		fi
        if [ -f "../tmp5/DORAX_ENVDATA_DESC_TRANSLATION_${lang}.csv" ]; then
			sed 1d ../tmp5/DORAX_ENVDATA_DESC_TRANSLATION_${lang}.csv >> masterdata/loc/XD_TRANSLATION_${lang}.csv
		fi
		if [ ! -z "$1" ]; then
			echo "Patch meta.xml"
			mv meta.xml meta.xml.orig
			cat meta.xml.orig | ../../helper/patch.exe "$1</td:version>" "$version</td:version>" > meta.xml
		fi
 		zip -r ../LOC-TXT-SD-$version-$lang.zip meta.xml masterdata
		rm -rf meta.xml masterdata
	fi
done
