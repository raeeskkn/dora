import java.util.zip.*


class Test {

   String param1 = "20200219"
   String param2 = "../../../dora/LOC-TXT-SD-PKW/19-85/LOC-TXT-SD-PKW-19-85.zip"
   String param3 = "../../../dora/LOC-TXT-SD-PKW/19-101/LOC-TXT-SD-PKW-19-101.zip"
   String param4 = "../../../dora/LOC-TXT-SD-VAN/19-101/LOC-TXT-SD-VAN-19-101.zip"
   String param5 = "../../../dora/LOC-TXT-SD-AED/20200130/LOC-TXT-SD-AED-20200130.zip"

   String zipFileName = "../../../dora/LOC-TXT-SD-AED/20200130/LOC-TXT-SD-AED-20200130.zip"

   //def outputDir = "tmp5"


   public void convertToMasterData() {

      //create temperary directory, extract zip files, rename one file
      Boolean isExtracted2 = unzipAndRename(param2, 'tmp2')
      Boolean isExtracted3 = unzipAndRename(param3, 'tmp3')
      Boolean isExtracted4 = unzipAndRename(param4, 'tmp4')
      Boolean isExtracted5 = unzipAndRename(param5, 'tmp5')

      //check newer version if any
      String newversion = (!param1.equalsIgnoreCase(null)) ? new File(System.getProperty("user.dir")).getName() : null
      println(newversion)

      //Convert to master data

   }


   public Boolean unzipAndRename(String zipFilePath, String newDirName) {
      if(zipFilePath?.trim()) {
         final File newDir = new File(newDirName)
         if (newDir.exists()) {
            newDir.deleteDir()
         }
         newDir.mkdirs()
         isUnzipFiles(zipFilePath, newDir.getName())
         File dorax_translation
         Boolean result
         if(newDir.getName().equalsIgnoreCase("tmp5")) {
            dorax_translation = new File(newDir.getAbsolutePath() + File.separator + "DORAX_ENVDATA_DESC_TRANSLATION_sh.csv");
            result = (dorax_translation.isFile()) ? dorax_translation.renameTo(new File(newDir.getAbsolutePath() + File.separator + "DORAX_ENVDATA_DESC_TRANSLATION_hr.csv")) : false
         } else {
            dorax_translation = new File(newDir.getAbsolutePath() + File.separator + "DORAX_TRANSLATION_sh.csv");
            result = (dorax_translation.isFile()) ? dorax_translation.renameTo(new File(newDir.getAbsolutePath() + File.separator + "DORAX_TRANSLATION_hr.csv")) : false
         }
         return result

      }
   }

   //Zip files
   public void getZipFile() {
      ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(zipFileName))
      new File(baseDir).eachFile() { file ->
         //check if file
         if (file.isFile()){
            zipFile.putNextEntry(new ZipEntry(file.name))
            def buffer = new byte[file.size()]
            file.withInputStream {
               zipFile.write(buffer, 0, it.read(buffer))
            }
            zipFile.closeEntry()
         }
      }
      zipFile.close()
   }



   //UnZip archive
   public void isUnzipFiles(String zipFilePath, String outputDir) {
      def zip = new ZipFile(new File(zipFilePath))
      zip.entries().each{
         if (!it.isDirectory()){
            def fOut = new File(outputDir+ File.separator + it.name)
            //create output dir if not exists
            new File(fOut.parent).mkdirs()
            try {
               def fos = new FileOutputStream(fOut)
               def buf = new byte[it.size]
               def len = zip.getInputStream(it).read(buf)
               //println zip.getInputStream(it).text
               fos.write(buf, 0, len)
               fos.close()
            } catch (FileNotFoundException e) {
               e.printStackTrace();
            } catch (IOException e) {
               e.printStackTrace();
            }
         }
      }
      zip.close()
   }


   static void main(String[] args) {
      Test unZipObj = new Test();
      unZipObj.convertToMasterData();
   }

}

